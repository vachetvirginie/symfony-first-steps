<?php

namespace OC\PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OC\PlatformBundle\Entity\Advert;
class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('OCPlatformBundle:Default:index.html.twig');
    }
}
