<?php

/* OCPlatformBundle:Advert:form.html.twig */
class __TwigTemplate_0b948d969d019d6474540e4f96e829414a4d9f6e83fc5d4d223fd1121b84283f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "OCPlatformBundle:Advert:form.html.twig"));

        // line 2
        echo "

";
        // line 9
        echo "

<h3>Formulaire d'annonce</h3>


";
        // line 17
        echo "
<div class=\"well\">

  Ici se trouvera le formulaire.

</div>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OCPlatformBundle:Advert:form.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  33 => 17,  26 => 9,  22 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# src/OC/PlatformBundle/Resources/views/Advert/form.html.twig #}


{# Cette vue n'hérite de personne, elle sera incluse par d'autres vues qui,

   elles, hériteront probablement du layout. Je dis « probablement » car,

   ici pour cette vue, on n'en sait rien et c'est une info qui ne nous concerne pas. #}


<h3>Formulaire d'annonce</h3>


{# On laisse vide la vue pour l'instant, on la comblera plus tard

   lorsqu'on saura afficher un formulaire. #}

<div class=\"well\">

  Ici se trouvera le formulaire.

</div>", "OCPlatformBundle:Advert:form.html.twig", "/home/vivi/dev/symfony/src/OC/PlatformBundle/Resources/views/Advert/form.html.twig");
    }
}
